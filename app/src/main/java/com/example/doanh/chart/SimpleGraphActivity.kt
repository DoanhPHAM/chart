package com.example.doanh.chart

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jjoe64.graphview.series.LineGraphSeries
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import kotlinx.android.synthetic.main.activity_simple_graph.*


class SimpleGraphActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_graph)

        val data = arrayOf(DataPoint(0.00, 1.00), DataPoint(1.00, 5.00), DataPoint(2.00, 3.00), DataPoint(3.00, 2.00), DataPoint(4.00, 6.00))
        val series = LineGraphSeries(data)
        graph.addSeries(series)
    }
}
