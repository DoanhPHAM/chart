package com.example.doanh.chart

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.activity_main.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class MainActivity : AppCompatActivity() {
    private var data = ArrayList<Word>()
    private val highlights = ArrayList<Highlight>()

    companion object {
        const val TAG = "Chart"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        initChart()
    }

    private fun initChart() {
        data = createDumpData()
        val values = ArrayList<Entry>()

        var start = data[0].start - 20
        if (start < 0) {
            start = 0
        }

        values.add(Entry(start.toFloat(), 0f))
        for (word in data) {
            val x = ((word.start + word.end) / 2)
            val y = (5..8).shuffled().last().toFloat()

            word.avg = x
            values.add(Entry(x.toFloat(), y))
            highlights.add(Highlight(x.toFloat(), y, 0))
        }

        val dataSet = LineDataSet(values, "Data set")
        dataSet.apply {
            setDrawValues(false)
            setDrawIcons(false)
            setDrawCircleHole(false)
            setDrawCircles(false)
            setDrawFilled(true)
            setDrawHorizontalHighlightIndicator(false)
            isHighlightEnabled = true
            mode = LineDataSet.Mode.CUBIC_BEZIER
            fillColor = getColor(R.color.orange)
            color = getColor(R.color.red)
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(dataSet)
        val lineData = LineData(dataSets)

        val xAxis = chart.xAxis
        xAxis.apply {
            setDrawGridLines(true)
            setDrawAxisLine(true)
            setDrawLabels(false)
            axisMinimum = start.toFloat()
            gridColor = Color.parseColor("#EEEEEE")
            isEnabled = true
            position = XAxis.XAxisPosition.BOTTOM
        }


        val yAxis = chart.axisLeft
        yAxis.apply {
            setDrawAxisLine(false)
            setDrawGridLines(true)
            isEnabled = false
        }

        chart.data = lineData
        chart.apply {
            description.isEnabled = false
            legend.isEnabled = false
            axisRight.isEnabled = false
            setVisibleXRangeMaximum(150f)
            setDrawGridBackground(false)
            setBackgroundColor(Color.TRANSPARENT)
            setHardwareAccelerationEnabled(true)
        }

        val vto = chart_wrapper.viewTreeObserver
        vto.addOnGlobalLayoutListener {
            val height = chart_wrapper.measuredHeight

            val marker = SpeakChartHighlight(this, R.layout.item_highlight, data, height)
            chart.marker = marker
            chart.highlightValues(highlights.toTypedArray())
        }
    }

    private fun createDumpData(): ArrayList<Word> {
        val words = ArrayList<Word>()
        words.add(Word(0.847f, "わたし", 88, 124))
        words.add(Word(0.651f, "の", 125, 146))
        words.add(Word(0.053f, "アパート", 147, 184))
        words.add(Word(0.81f, "で", 185, 195))
        words.add(Word(0.792f, "は", 196, 229))
        words.add(Word(0.549f, "ゴミ", 259, 283))
        words.add(Word(0.884f, "の", 284, 294))
        words.add(Word(0.204f, "ひ", 295, 312))
        words.add(Word(0.714f, "は", 313, 349))
        words.add(Word(0.164f, "ねつ", 378, 401))
        words.add(Word(0.095f, "よう", 402, 419))
        words.add(Word(0.082f, "びん", 420, 437))
        words.add(Word(0.258f, "と", 438, 447))
        words.add(Word(0.056f, "も", 448, 465))
        words.add(Word(0.991f, "すいよう", 493, 540))
        words.add(Word(0.153f, "ひ", 541, 551))
        words.add(Word(0.49f, "に", 552, 558))
        words.add(Word(0.019f, "ど", 559, 588))
        words.add(Word(0.997f, "きんよう", 627, 670))
        words.add(Word(0.329f, "ひ", 671, 682))
        words.add(Word(0.56f, "です", 683, 717))

        return words
    }

}