package com.example.doanh.chart

data class Word(
        val accuracy: Float,
        val char: String,
        val start: Int,
        val end: Int,
        var shown: Boolean = false,
        var avg: Int = 0,
        val color: Int = (0..2).shuffled().first()
)