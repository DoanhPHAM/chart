package com.example.doanh.chart

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Color
import android.util.Log
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.item_highlight.view.*


@SuppressLint("ViewConstructor")
class SpeakChartHighlight(
        context: Context,
        layoutResource: Int,
        private val words: ArrayList<Word>,
        private val chartHeight: Int
) : MarkerView(context, layoutResource) {
    private var mOffset: MPPointF? = null
    companion object {
        private const val BLUE = "#4992DD"
        private const val GREEN = "#7DD241"
        private const val WHITE = "#FFFFFF"
        private var textWidth = 0

        private val colors = arrayOf(BLUE, WHITE, GREEN)
    }

    override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
        super.draw(canvas, posX, chartHeight.toFloat())
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        val value = e!!.x.toInt()
        var found = false
        for (word in words) {
            if (value == word.avg) {
                highlight_value.text = word.char
                found = true

                val color = colors[word.color]
                highlight_value.backgroundTintList = ColorStateList.valueOf(Color.parseColor(color))
                highlight_value.setTextColor(Color.parseColor("#212121"))
                if (color == BLUE || color == GREEN) {
                    highlight_value.setTextColor(Color.parseColor(WHITE))
                }
            }
        }

        if (!found) {
            highlight_value.text = value.toString()
        }

        super.refreshContent(e, highlight)

//        val vto = highlight_value.viewTreeObserver
//        vto.addOnGlobalLayoutListener {
//            val textWidth = highlight_value.measuredWidth
//            val w = -textWidth / 2
//            val h = -height
//
//            mOffset = MPPointF(w.toFloat(), h.toFloat())
//            setOffset(w.toFloat(), h.toFloat())
//        }
    }

    override fun getOffset(): MPPointF? {
        if (mOffset == null) {
            // center the marker horizontally and vertically
            val w = -width/2
            val h = -height

            mOffset = MPPointF(w.toFloat(), h.toFloat())
        }

        return mOffset
    }
}